
			</main>
			<!--/ page content -->
				
				
			<!-- page footer -->
			<footer class="page-footer">
				<div class="grid-con">
					<div class="copyrights">
						<p>© 2015 Mena Associates. All rights reserved.</p>
						<p><a href="http://viral21.com/" target="_blank">Web design and development by <span>VIRAL21</span></a></p>
					</div>
				</div>
			</footer>
			<!--/ page footer -->
		</div>
		<!--/ page -->

		
		
		
		<!-- scripts -->
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.2.0.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.isotope.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
		<!--/ scripts -->
		<?php wp_footer(); ?>
	</body>
</html>

