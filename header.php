
<!DOCTYPE html>
<html<?php language_attributes(); ?>>
	<head>
		<title></title>
			
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		
		<!-- styles -->
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css">
		<!--/ styles -->
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<!-- page -->		
		<div class="page">
			<!-- page header -->
			<header id="page-header" class="page-header">
				<!-- logo -->
				<a href="index.html" class="logo"><img src="<?php bloginfo('template_directory'); ?>/img/page-header/logo.png" width="196" height="40" alt=""></a>
				<!--/ logo -->
							
				<!-- lang nav -->
				<nav class="lang-nav">
					<select>
						<option value="eng" selected>English</option>
						<option value="deu">Deutsch</option>
					</select>
					<span>ENG</span>
				</nav>
				<!--/ lang nav -->
						
				<!-- main nav -->
				<nav class="main-nav">
					<ul>
						<?php wp_list_pages('title_li=&current_page_item='); ?>
					</ul>
				</nav>
				<!--/ main nav -->
								
				<!-- mobile nav -->
				<nav id="mobile-nav" class="mobile-nav">
					<div class="table">
						<div class="cell">
							<ul>
								<?php wp_list_pages('title_li=&current_page_item='); ?>
							</ul>
						</div>
					</div>
				</nav>
				<!--/ mobile nav -->
								
				<!-- burger -->
				<button id="burger" class="burger"><i></i></button>
				<!--/ burger -->
			</header>
			<!--/ page header -->
					
            <!-- page content -->
	        <main class="page-content">