<?php

class cpt{
	public $name;
	public $label;
	public $text_domain;
	public $supports;

	function __construct($name,$label,$text_domain,$supports){
		$this->name=$name;
		$this->label=$label;
		$this->text_domain=$text_domain;
		$this->supports=$supports;
		add_action('init',array($this,'create_posttype'));
	}

	function create_posttype(){
		register_post_type($this->name,array(
			'labels'=>array(
			    	'name'=>__(ucfirst($this->name)),
			    	'singular_name'=>__(ucfirst($this->label)),
			    	'search_items'      => __( 'Search '.ucfirst($this->label), $this->text_domain),
					'all_items'         => __( 'All '.ucfirst($this->label)."s", $this->text_domain),
					'parent_item'       => __( 'Parent '.ucfirst($this->label) , $this->text_domain),
					'parent_item_colon' => __( 'Parent :'.ucfirst($this->label) , $this->text_domain),
					'edit_item'         => __( 'Edit '.ucfirst($this->label) , $this->text_domain),
					'update_item'       => __( 'Update '.ucfirst($this->label), $this->text_domain),
					'add_new_item'      => __( 'Add New '.ucfirst($this->label) , $this->text_domain),
					'new_item_name'     => __( 'New '.ucfirst($this->label), $this->text_domain ),
					'menu_name'         => __( ucfirst($this->label), $this->text_domain)
			    ),
			    'public'=>true,
			    'has_archive'=>true,
			    'supports'=>$this->supports,
			    'rewrite'=>array('slug'=>$this->label),
			    'menu_position' => 5,
			));
	}

	function create_taxonomy($lable) {
		    $newlable = str_replace("_", " ", $lable);
			$labels = array(
					'name'              => __( ucfirst($newlable) , $this->text_domain),
					'singular_name'     => __( ucfirst($newlable), $this->text_domain),
					'search_items'      => __( 'Search '.ucfirst($newlable), $this->text_domain),
					'all_items'         => __( 'All '.ucfirst($newlable)."s", $this->text_domain),
					'parent_item'       => __( 'Parent '.ucfirst($newlable) , $this->text_domain),
					'parent_item_colon' => __( 'Parent :'.ucfirst($newlable) , $this->text_domain),
					'edit_item'         => __( 'Edit '.ucfirst($newlable) , $this->text_domain),
					'update_item'       => __( 'Update '.ucfirst($newlable), $this->text_domain),
					'add_new_item'      => __( 'Add New '.ucfirst($newlable) , $this->text_domain),
					'new_item_name'     => __( 'New '.ucfirst($newlable), $this->text_domain ),
					'menu_name'         => __( ucfirst($newlable), $this->text_domain),
				);

				$args = array(
					'hierarchical'      => true,
					'labels'            => $labels,
					'show_ui'           => true,
					'show_admin_column' => true,
					'query_var'         => true,
					'rewrite'           => array( 'slug' => $this->name),
				);

				register_taxonomy($lable, array( $this->name), $args );
		}

}

$supports=array('title');

$home_slideshow=new cpt('home_slideshow','home_slideshow','translation',array('title','editor'));

$lawyers=new cpt('lawyers','lawyer','translation',array('title','thumbnail','editor'));

$lawyers->create_taxonomy('practice_areas');

$lawyers->create_taxonomy('languages');

$client_statement=new cpt('client_statement','client_statement','translation',array('title','thumbnail','editor'));

$expertise=new cpt('expertise','expertise','translation',array('title','thumbnail','editor'));

$resource=new cpt('resource_cpt','resource','translation',array('title','thumbnail','editor'));

$resource->create_taxonomy('resource_categories');

$office=new cpt('office_cpt','office','translation',array('title','thumbnail','editor'));

