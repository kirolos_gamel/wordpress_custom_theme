var headline;
var winScroll = 0;
var winHeight = 0;

/**/
/* on document load */
/**/
$(function()
{
	header = $('#page-header');
	headline = $('.headline h1');
	winHeight = $(window).height();
	
	
	/**/
	/* popups */
	/**/
	$('.popup-opener').on('click', function()
	{
		$($(this).attr('href')).addClass('active');
		return false;
	});
	$('.popup-closer').on('click', function()
	{
		$('.popup').removeClass('active');
		return false;
	});
	
	
	/**/
	/* select */
	/**/
	$('.select').on('change', 'select', function()
	{
		elem = $(this);
		elem.next().text(elem.find('option:selected').text());
	});
	
	
	/**/
	/* burger */
	/**/
	$('#burger').on('click', function()
	{
		$('#mobile-nav').toggleClass('active');
	});
	
	
	/**/
	/* reviews */
	/**/
	$('#reviews').owlCarousel({
		items: 1,
		loop: true,
		margin: 30,
		autoHeight: true,
		autoplay: true
	});
	
	
	/**/
	/* carousel */
	/**/
	$('.carousel').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		autoHeight: true,
		navText: ['','']
	});
});


/**/
/* on window load */
/**/
$(window).load(function()
{
	/**/
	/* lawyers */
	/**/
	$('#lawyers-grid').isotope({
		itemSelector: '.isotope-item',
		percentPosition: true,
		masonry: {
			columnWidth: '.isotope-sizer'
		}
	});
	$('#lawyers-filter').on('change', 'select', function()
	{
		var filter = '';
		$('#lawyers-filter select').each(function()
		{
			if( $(this).val() != '*' )
				filter = filter + $(this).val();
		});
		$('#lawyers-grid').isotope({filter: filter});
	});
});


/**/
/* on window scroll */
/**/
$(window).scroll(function()
{
	winScroll = $(this).scrollTop();
	if( winScroll > 0 )
	{
		header.addClass('fixed');
	}
	else
	{
		header.removeClass('fixed');
	}
	headline.css({'opacity': (1 - winScroll/500)}).css({'top': winScroll/2});
});


/**/
/* on window resize */
/**/
$(window).resize(function()
{
});