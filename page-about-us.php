<?php get_header(); ?>

    <?php
		if(have_posts()):
		  while(have_posts()):
		  	the_post();
		    //first feature image
		  	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		  	//title we divide it
		  	$string=get_post_meta( get_the_ID(), 'main_title', true );
			$arr=explode(' ',$string,2); 
			//Image
			$url= get_post_meta( get_the_ID(), 'center_image', true );
			//echo "<div>".$arr[0]."</div>";
    ?>
       <div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-2.jpg'; endif; ?>)">
			<div class="grid-con">
				<div class="table">
					<div class="cell">
						<h1><?php echo $arr[0]; ?><span><?php echo $arr[1]; ?></span></h1>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="grid-con">
			<div class="grid-row">
				<div class="grid-col grid-col-8 grid-col-sm-12">
					<!-- about -->
					<div class="block wysiwyg about">
						<img src="<?php if($url): echo $url; else: echo bloginfo('template_directory').'/img/about/logo.png'; endif; ?>" width="578" height="139" alt="">
						<p><?php the_content(); ?></p>
						<h2>Who We Are</h2>
						<p><?php echo get_post_meta( get_the_ID(), 'who_we_are', true ); ?></p>
						<h2>Our Mission</h2>
						<p><?php echo get_post_meta( get_the_ID(), 'our_mission', true ); ?></p>
					</div>
					<!--/ about -->
				</div>
				
				
				<div class="grid-col grid-col-4 grid-col-sm-12">
					<!-- practice areas -->
					<nav class="widget practice-areas">
						<h4><span>Practice Areas</span></h4>
						<?php
							$args=array("post_type"=>"expertise");
							$expertise=new WP_Query($args);
							if($expertise->have_posts()):  
			   	            ?>
							<ul>
								<?php 
								 while($expertise->have_posts()):
								 	$expertise->the_post();
								    $current_id=get_the_ID();
								?>
							      <li class="<?php if($current_id==$id_post): echo 'active';  endif; ?>"><a  href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php
								 endwhile;
								?>
							</ul>
							<?php
							  endif;
						?>
					</nav>
					<!--/ practice areas -->
					<?php endwhile; endif; ?>
					
					<!-- testimonials -->
					<div id="testimonials" class="widget testimonials">
						<h4><span>Client Statements</span></h4>
						<div class="carousel">
							<?php
							    $args=array("post_type"=>"client_statement");
					            $clients_slideshow=new WP_Query($args);
					            if($clients_slideshow->have_posts()):
					            	while($clients_slideshow->have_posts()):
					            		$clients_slideshow->the_post();
					            	    $client_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
							?>
							<div class="item">
								<div class="pic">
									<img src="<?php echo $client_image[0]; ?>" width="165" height="54" alt="">
								</div>
								<p><?php the_content(); ?></p>
								<h5><?php the_title(); ?></h5>
								<h6><?php echo get_post_meta( get_the_ID(), 'job', true ); ?></h6>
							</div>
							 <?php
						         endwhile;
						       endif;
						    ?>
						</div>
					</div>
					<!--/ testimonials -->
				</div>
			</div>
		</div>
		

<?php  get_footer(); ?>			