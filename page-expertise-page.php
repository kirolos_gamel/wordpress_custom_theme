<?php
get_header();
?>
<?php
	if(have_posts()):
	   while(have_posts()):
		the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>		  	
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-3.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>


<!-- services -->
<div class="services">
<?php
$args=array("post_type"=>"expertise");
$expertise=new WP_Query($args);
if($expertise->have_posts()):
	$i=0;
   while($expertise->have_posts()):
   	$expertise->the_post();
    $image_expertise = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
    ++$i;
    if(isPrime($i)):
?>
	<div class="item">
		<div class="grid-con">
			<div class="table">
				<div class="cell pic">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $image_expertise[0]; ?>" width="470" height="470" alt=""></a>
				</div>
				<div class="cell wysiwyg">
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<div class="content_description">
						<?php  the_excerpt(); ?>
					</div>
					<a href="<?php the_permalink(); ?>" class="button">View Details and Projects<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		</div>
	</div>

<?php
//check if prime or not prime number
else:
?>

<div class="item">
	<div class="grid-con">
		<div class="table">
			<div class="cell wysiwyg">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<div class="content_description">
					<?php the_excerpt(); ?>
				</div>
				<a href="<?php the_permalink(); ?>" class="button">View Details and Projects<i class="fa fa-angle-right"></i></a>
			</div>
			<div class="cell pic">
			    <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_expertise[0]; ?>" width="470" height="470" alt=""></a>
			</div>
			
		</div>
	</div>
</div>

<?php endif; endwhile; endif; ?>	
	

</div>
<!--/ services -->
<?php
get_footer();
?>