
<?php get_header(); ?>

		<?php
		  if(have_posts()):
		  	while(have_posts()):
		  		the_post();
		  	    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>
		<!-- intro -->
		<div class="intro" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-1.jpg'; endif; ?>)" >
			<div class="table">
				<div class="cell">
					<h1>
						<?php 
						   $string=get_post_meta($post->ID, 'head', true);
						   $arr=explode(' ',$string,2); 
						?>
						<span class="item-1"><?php echo $arr[0]; ?></span>
						<span class="item-2"><?php $rr=explode(',',$arr[1],2); echo $rr[0];
						//IS A TREASURE, BUT
						?></span>
						<span class="item-3">PRACTICE</span>
						<span class="item-4">IS THE KEY TO IT</span>
					</h1>
					<a href="about.html" class="button button-white">Learn about us<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
			<div class="reviews">
				<div id="reviews">
					<?php 
						$args=array("post_type"=>"home_slideshow");
					    $reviews=new WP_Query($args);
					    if($reviews->have_posts()):
					    	while($reviews->have_posts()):
					    		$reviews->the_post();
					?>
                        <div class="item">
                        	<p><?php the_content(); ?></p>
                        	<h5><?php echo get_post_meta( get_the_ID(), 'head', true ); ?></h5>
                        	<h6><?php the_title(); ?></h6>
                        </div>
				    <?php
				         endwhile;
				       endif;
				    ?>
				</div>
			</div>
		</div>
		<!--/ intro -->

	<?php endwhile; endif;  ?>


			</main>
			<!--/ page content -->
		<!-- scripts -->
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.2.0.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.isotope.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
		<!--/ scripts -->
		<?php wp_footer(); ?>
	</body>
</html>

