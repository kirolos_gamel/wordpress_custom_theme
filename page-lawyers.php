<?php get_header();?>
       <?php
		  if(have_posts()):
		  	while(have_posts()):
		  		the_post();
		  	    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>
    <!-- headline -->
	<div class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-6.jpg'; endif; ?>)">
		<div class="grid-con">
			<div class="table">
				<div class="cell">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	<!--/ headline -->
	<?php
	  endwhile;
	endif;
	?>
	<!-- lawyers -->
	<div class="block lawyers">
		<div class="grid-con">
			<div id="lawyers-filter" class="filter">
				<div class="grid-row">
					<div class="grid-col grid-col-6 removed-md">
						<h4>Quick Lawyer Search</h4>
					</div>
					<div class="grid-col grid-col-3 grid-col-md-6 grid-col-sm-12">
						<div class="select">
						<?php
							$terms_practice= get_terms( 'practice_areas', array(
							    'hide_empty' => false,
							));
						?>
							<select>
								<option value="*">All Practice Areas</option>
								<?php 
								  foreach($terms_practice as $practice_area):
								?>
								<option value="<?php echo '.pa-'.$practice_area->term_id; ?>"><?php echo $practice_area->name; ?></option>	
							    <?php endforeach; ?>
							</select>
							<span>All Practice Areas</span>
						</div>
					</div>
					<div class="grid-col grid-col-3 grid-col-md-6 grid-col-sm-12">
						<div class="select">
						<?php
							$terms_languages= get_terms( 'languages', array(
							    'hide_empty' => false,
							));
						?>
							<select>
								<option value="*">All Languages</option>
								<?php 
								  foreach($terms_languages as $language):
								?>
								<option value="<?php echo '.lg-'.$language->term_id; ?>"><?php echo $language->name; ?></option>	
							    <?php endforeach; ?>
							</select>
							<span>All Languages</span>
						</div>
					</div>
				</div>
			</div>
			
			<div id="lawyers-grid" class="isotope">
				<?php
				  $args=array("post_type"=>"lawyers");
					    $lawyers=new WP_Query($args);
					    if($lawyers->have_posts()):
					    	while($lawyers->have_posts()):
					    		$lawyers->the_post();
					    	    $practice_areas_law=get_the_terms( $post->ID, 'practice_areas' );
					    	    $languages_law=get_the_terms( $post->ID, 'languages' );
					    	    $image_lawyer = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				?>
				<div class="isotope-item <?php foreach($practice_areas_law as $prac): echo "pa-".$prac->term_id." "; endforeach; foreach($languages_law as $lang): echo "lg-".$lang->term_id." "; endforeach; ?> ">
					<a href="#<?php the_ID(); ?>" class="item popup-opener">
						<img src="<?php echo $image_lawyer[0]; ?>" width="270" height="374" alt="Image Lawyer">
						<div class="info">
							<h3><?php the_title(); ?></h3>
						</div>
					</a>
					<!-- popup -->
					<div id="<?php the_ID(); ?>" class="popup popup-laweyer">
						<div class="popup-closer over"></div>
						<div class="table">
							<div class="cell">
								<div class="inner">
									<div class="grid-row">
										<div class="grid-col grid-col-3 removed-sm">
											<img src="<?php echo $image_lawyer[0]; ?>" width="195" height="270" alt="">
											<div class="wysiwyg">
												<p>T: <span><?php echo get_field('telephone'); ?></span><br>E: <a href="#"><?php echo get_field('email'); ?></a></p>
											</div>
										</div>
										<div class="grid-col grid-col-9 grid-col-sm-12">
											<div class="wysiwyg">
												<h2><?php the_title(); ?></h2>
												<p class="content">
													<?php the_content(); ?>
												</p>
												<h4>Practice Areas</h4>
												<ul>
													<?php
													    $practice_areas=wp_get_post_terms($post->ID,'practice_areas');
													    //print_r($practice_areas);
													    foreach($practice_areas as $area):
													?>
				                                     <li><?php echo $area->name; ?></li>
				                                    <?php endforeach; ?> 
											    </ul>

											    <h4>Languages</h4>
												<p>
													<?php
													    $languages=wp_get_post_terms($post->ID,'languages');
													    //print_r($practice_areas);
													    $num=count($languages);
													    $b=0;
													    foreach($languages as $language):
				                                          ++$b;
													?>
				                                     <?php if($b==($num)): echo $language->name; else: echo $language->name.", "; endif; ?>
				                                    <?php endforeach; ?> 
											    </p>

												<h4>Publications</h4>
												<p>
												  <?php echo get_field('publications'); ?>
												</p>  
											</div>
										</div>
									</div>
									<button type="button" class="popup-closer cross"></button>
								</div>
							</div>
						</div>
					</div>
					<!--/ popup -->
				</div> 

				<?php
				      endwhile;
				    endif;
				?>
				
				<div class="isotope-sizer"></div>
			</div>
		</div>
	</div>
	<!--/ lawyers -->



 <?php get_footer(); ?>