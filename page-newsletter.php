<?php
get_header();
?>
<?php
	if(have_posts()):
	   while(have_posts()):
		the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>		  	
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-7.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>

<div class="grid-con">
	<div class="grid-row">
		<div class="grid-col grid-col-8 grid-col-sm-12">
			<!-- blog -->
			<div class="blog">
				<?php
				    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$posts_per_page=get_option('posts_per_page ');
					//global $wp_query;
					$args=array('category_name' => 'newsletter',
						'posts_per_page' => $posts_per_page,
	   					'paged' => $paged);
					$newsletter=new WP_Query($args);
					$pages_num = $newsletter->max_num_pages;
                    //echo "<h1>number pages: ".$pages_num."</h1>";
					if($newsletter->have_posts()):
					   while($newsletter->have_posts()):
					   	$newsletter->the_post();
					    $image_newsletter = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

				?>
				<div class="block item">
					<a href="<?php the_permalink(); ?>" class="pic"><img src="<?php echo $image_newsletter[0]; ?>" width="200" height="200" alt=""></a>
					<div class="wysiwyg newsletter-content">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<div class="date"><?php the_date(); ?></div>
						<?php the_excerpt(); ?>
						<a class="read-more" href="<?php the_permalink(); ?>">read more<i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
			<?php endwhile; endif; ?>
			</div>
			<!--/ blog -->
						
			<!-- pager -->
			<div class="block pager">
				<?php
				  if($pages_num!=1):
				?>  	
				<a href="<?php echo get_pagenum_link($paged - 1); ?>" class="<?php if(($paged-1==0)): echo "disabled"; endif; ?>"><i class="fa fa-chevron-left"></i></a>
				<?php
				    for($p=1;$p<=$pages_num;$p++):
				?>
			    <a href="<?php echo get_pagenum_link($p); ?>" class="<?php if($p==$paged):echo "active"; endif; ?>"><?php echo $p; ?></a> 
                <?php
                   endfor;
                 ?>
				<a href="<?php echo get_pagenum_link($paged + 1); ?>" class="<?php if(($paged+1>$pages_num)): echo "disabled"; endif; ?>"><i class="fa fa-chevron-right"></i></a>
				<?php
				 elseif($pages_num==1):
                ?>
                <a class="active" href="<?php echo get_pagenum_link(1); ?>"><?php echo "1"; ?></a> 
               <?php endif; ?>
			</div>
			<!--/ pager -->
		</div>
		
		
		<div class="grid-col grid-col-4 grid-col-sm-12">
			<!-- search -->
			<form class="widget search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
				<input type="hidden" name="post_type" value="post" />
				<div class="input"><input name="s" id="s" type="text" placeholder="Search Newsletter..."></div>
				<button type="submit" class="button"><i class="fa fa-search"></i></button>
			</form>
			<!--/ search -->
			
			
			<!-- categories -->
			<nav class="widget practice-areas">
				<h4><span>Categories</span></h4>
				<ul>
				<?php
				 $terms = get_terms('category',array(
				    'hide_empty' => false,
				));
				 if($terms):
                 ?>
                  <li class="active"><a href="#">Show All</a></li>
                 <?php
					 foreach($terms as $term_cat):
					 	if($term_cat->name=="newsletter"):
					 		$id_parent=$term_cat->term_id;
					 	endif;
					 endforeach;
					 foreach($terms as $term_cat):
					 	if($term_cat->parent==$id_parent):
				?>
			       <li><a href="#"><?php echo $term_cat->name; ?></a></li>
			   <?php endif; endforeach; ?>
			    <?php
			    else: 
			    ?>
			     <li class="active"><a href="#">No Categories Found.</a></li>
			    <?php endif; ?>
			    </ul>
			</nav>
			<!--/ categories -->
			
			
			<!-- subscription -->
			<div class="widget subscription">
				<h4><span>Stay In Touch</span></h4>
				<p>Subscribe below to get alerts, news, info and publications from Mena Associates</p>
				<?php echo do_shortcode("[caldera_form id='CF57c841eb1c706']"); ?>
			</div>
			<!--/ subscription -->
		</div>
	</div>
</div>

<?php get_footer(); ?>