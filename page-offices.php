<?php
get_header();
?>
<?php
	if(have_posts()):
	   while(have_posts()):
		the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>	
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-1.jpg'; endif; ?>)">
	<div id="headline-map" class="map"></div>
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>

<!-- offices -->
<div class="offices">
	<div class="grid-con">
		<div class="grid-row">
		<?php
			$args=array("post_type"=>"office_cpt");
			$office=new WP_Query($args);
			if($office->have_posts()):
			   $positions=array();
			   while($office->have_posts()):
			   	$office->the_post();
			    $one_position=get_post_meta(get_the_ID(),'map', true );
			    array_push($positions,$one_position);
			    $image_office = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>
			<div class="grid-col grid-col-4 grid-col-lg-6 grid-col-sm-12">
				<div class="block item">
					<a href="<?php echo the_permalink(); ?>" class="pic"><img src="<?php echo $image_office[0]; ?>" width="88" height="88" alt="Office Image"></a>
					<div class="wysiwyg">
						<h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p class="name"><?php echo get_field('name'); ?></p>
						<p>Tel.: <span><?php echo get_field('telephone'); ?></span></p>
						<?php $fax=get_field('fax');if($fax): ?><p>Fax: <span><?php echo get_field('fax'); ?></span></p><?php endif; ?>
						<p>Email: <a href="#"><?php echo get_field('email'); ?></a></p>
						<a href="<?php echo the_permalink(); ?>" class="button">Details<i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		<?php endwhile; endif; ?>
		</div>
	</div>
</div>
<!--/ offices -->

<!-- map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwuzxaNw1tAbz-grG-Bl_oAonZl9gTkMo"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/richmarker.min.js"></script>
<script>
	var map;
	
	map = new google.maps.Map(document.getElementById('headline-map'),
	{
		zoom: 3,
		scrollwheel: false,
		disableDefaultUI: true,
		center: new google.maps.LatLng(38.2534491,31.2233592),
		styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
	});
	<?php
	for($v=0;$v<count($positions);$v++):
	?>
	new RichMarker({
		position: new google.maps.LatLng(<?php echo $positions[$v]['lat']; ?>,<?php echo $positions[$v]['lng']; ?>),
		map: map,
		flat: true,
		content: '<div class="pin"><a href="office.html"><img src="http://thumb7.shutterstock.com/display_pic_with_logo/578401/157079105/stock-photo-munich-germany-skyline-at-city-hall-157079105.jpg" alt=""></a></div>'
	});
	<?php
    endfor;
	?>
	
</script>

<?php
get_footer();
?>


   
