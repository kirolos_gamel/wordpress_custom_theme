<?php
get_header();
?>
<?php
	if(have_posts()):
	   while(have_posts()):
		the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>		  	
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-4.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<?php 
				   $string=get_post_meta($post->ID, 'title', true);
				   $arr=explode(' ',$string,2); 
				?>
				<h1><?php echo $arr[0]; ?><span><?php echo $arr[1]; ?></span></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>


<div class="grid-con">
	<div class="grid-row">
		<?php
			$args=array("post_type"=>"resource_cpt");
			$resources=new WP_Query($args);
			if($resources->have_posts()):
			   while($resources->have_posts()):
			   	$resources->the_post();
			    $image_resources = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>
		<div class="grid-col grid-col-8 grid-col-sm-12">
			<!-- resources -->
			<div class="block resources">
				<div class="item clearfix">
					<a href="#download" class="pic popup-opener"><img src="<?php echo $image_resources[0]; ?>" width="129" height="129" alt="Resource Image"></a>
					<div class="wysiwyg">
						<h3><a href="#download" class="popup-opener"><?php the_title(); ?></a></h3>
						<p class="author">by <a href="#laweyer-2" class="popup-opener"><?php the_author(); ?></a></p>
						<p><?php the_content(); ?></p>
						<a href="<?php echo get_field('file'); ?>" class="popup-opener button button-green">Free Download<i class="fa fa-angle-down"></i></a>
					</div>
				</div>
			</div>
			<!--/ resources -->
		</div>
	<?php endwhile; endif; ?>
		
		
		<div class="grid-col grid-col-4 grid-col-sm-12">
			<!-- search -->
			<form class="widget search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
				<input type="hidden" name="post_type" value="resource_cpt" />
				<div class="input"><input name="s" id="s" type="text" placeholder="Search publications..."></div>
				<button type="submit" class="button"><i class="fa fa-search"></i></button>
			</form>
			<!--/ search -->
			
			
			<!-- categories -->
			<nav class="widget practice-areas">
				<h4><span>Categories</span></h4>
				<?php
					$terms = get_terms( 'resource_categories', array(
					    'hide_empty' => false,
					) );
				//var_dump($terms);
				?>
				<ul>
				<?php
				    if($terms):
				?>	
			     <li class="active"><a href="#">Show All</a></li>
			        <?php
						foreach($terms as $term):
					?>
                      <li><a href="#"><?php echo $term->name; ?></a></li>
					<?php	
						endforeach;
					else:
					?>
				     <li class="active"><a href="#">No Categories Found.</a></li>
				   <?php endif; ?>
				</ul>
			</nav>
			<!--/ categories -->
			
			
			<!-- subscription -->
			<div class="widget subscription">
				<h4><span>Stay In Touch</span></h4>
				<p>Subscribe below to get alerts, news, info and publications from Mena Associates</p>
				<?php echo do_shortcode("[caldera_form id='CF57c841eb1c706']"); ?>
			</div>
			<!--/ subscription -->
		</div>
	</div>
</div>

<?php
get_footer(); 
?>