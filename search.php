<?php
get_header(); ?>


<?php

if($_GET["post_type"]=='resource_cpt'):
	$args=array('pagename' => 'resources');
    $resource_page=new WP_Query($args);
	if($resource_page->have_posts()):
	   while($resource_page->have_posts()):
	   	$resource_page->the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-4.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<?php 
				   $string=get_post_meta($post->ID, 'title', true);
				   $arr=explode(' ',$string,2); 
				?>
				<h1><?php echo $arr[0]; ?><span><?php echo $arr[1]; ?></span></h1>
			</div>
		</div>
	</div>
</div>
<?php
   endwhile; 
 endif; //if resource page has posts
//endif; //if for check custom post type
?>

<?php

elseif($_GET["post_type"]=='post'):
	$args=array('pagename' => 'newsletter');
    $newsletter_page=new WP_Query($args);
	if($newsletter_page->have_posts()):
	   while($newsletter_page->have_posts()):
	   	$newsletter_page->the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-7.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<?php
   endwhile; 
 endif; //if resource page has posts
endif; //if for check custom post type
?>
		<?php
		if ( have_posts() ) : ?>

		<div class="grid-con single-expertise">
			<div class="grid-row">
				<div class="grid-col grid-col-12 grid-col-sm-12">
				<!-- service -->
				    <?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
						$image_search = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			        ?>
					<div class="block wysiwyg service" id="post-<?php the_ID(); ?>">
				      <h1><?php printf( esc_html__( 'Search Results for: %s', '_s' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				      <h3><?php the_title(); ?></h3>
				      <h4><?php the_date(); ?> </h4>
				      <div class="img_container">
				      	<img src="<?php echo $image_search[0]; ?>" />
				      </div>
			          <div class="content">
					    <?php the_content(); ?>
				      </div>
				      <?php if($_GET["post_type"]=='resource_cpt'): ?>
				        <a href="<?php echo get_field('file'); ?>" class="popup-opener button-inside-search button-green">Free Download<i class="fa fa-angle-down"></i></a>
				      <?php endif; ?>
				    </div>  

            <?php
			endwhile;

		else :
        ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', '_s' ); ?>
    
	   <?php endif; ?>
     </div>
  </div>
</div>

<?php
get_footer();
