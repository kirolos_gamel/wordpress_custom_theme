<?php
get_header();
?>
<?php
    $args=array('pagename' => 'expertise-page');
    $expertise_page=new WP_Query($args);
	if($expertise_page->have_posts()):
	   while($expertise_page->have_posts()):
		$expertise_page->the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>		  	
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-3.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>


<?php
while ( have_posts() ) : the_post();
$id_post=get_the_ID();
?>
<div class="grid-con single-expertise">
	<div class="grid-row">
		<div class="grid-col grid-col-8 grid-col-sm-12">
			<!-- service -->
			<div class="block wysiwyg service">
				<h1>
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;
					?>
				</h1>
				<div class="content">
					<?php the_content(); ?>
				</div>

				<div class="projects">
					<h2>Projects</h2>
					<?php echo get_post_meta( get_the_ID(), 'projects', true ); ?>
			    </div>
			</div>
			<!--/ service -->


			<!-- lawyers -->
			<div class="block lawyers lawyers-alt">
				<div class="wysiwyg">
					<h2>Related attorneys</h2>
				</div>
				<div id="lawyers-grid" class="isotope">
					<?php
					 $newone=get_field('lawyers');
					 //var_dump($newone); 
					 for($i=0;$i<count($newone);$i++):
					 	//print_r($newone);
					    $image_lawyer = wp_get_attachment_image_src( get_post_thumbnail_id( $newone[$i]->ID ), 'single-post-thumbnail' );
					 ?>	
					
					<div class="isotope-item pa-ad pa-ccl pa-ip lg-arabic lg-english lg-german">
						<a href="#<?php echo $newone[$i]->ID; ?>" class="item popup-opener">
							<img src="<?php echo $image_lawyer[0]; ?>" width="270" height="374" alt="Lawyer Image">
							<div class="info">
								<h3><?php echo $newone[$i]->post_title; ?></h3>
							</div>
						</a>
					</div>
					<!-- popup -->
					<div id="<?php echo $newone[$i]->ID; ?>" class="popup popup-laweyer">
						<div class="popup-closer over"></div>
						<div class="table">
								<div class="cell">
									<div class="inner">
										<div class="grid-row">
											<div class="grid-col grid-col-3 removed-sm">
												<img src="<?php echo $image_lawyer[0]; ?>" width="195" height="270" alt="">
												<div class="wysiwyg">
													<p>T: <span><?php echo get_post_meta($newone[$i]->ID , 'telephone', true ); ?></span><br>E: <a href="#"><?php echo get_post_meta($newone[$i]->ID , 'email', true ); ?></a></p>
												</div>
											</div>
											<div class="grid-col grid-col-9 grid-col-sm-12">
												<div class="wysiwyg">
													<h2><?php echo $newone[$i]->post_title; ?></h2>
													<p class="content">
														<?php echo $newone[$i]->post_content; ?>
													</p>
													<h4>Practice Areas</h4>
													<ul>
														<?php
														    $practice_areas=wp_get_post_terms($newone[$i]->ID,'practice_areas');
														    //print_r($practice_areas);
														    foreach($practice_areas as $area):
														?>
					                                     <li><?php echo $area->name; ?></li>
					                                    <?php endforeach; ?> 
												    </ul>
												    <h4>Languages</h4>
													<p>
														<?php
														    $languages=wp_get_post_terms($newone[$i]->ID,'languages');
														    //print_r($practice_areas);
														    $num=count($languages);
														    $b=0;
														    foreach($languages as $language):
					                                          ++$b;
														?>
					                                     <?php if($b==($num)): echo $language->name; else: echo $language->name.", "; endif; ?>
					                                    <?php endforeach; ?> 
												    </p>
													<h4>Publications</h4>
													<p>
													 <?php  echo get_post_meta($newone[$i]->ID , 'publications', true ); ?>
												    </p>
												</div>
											</div>
										</div>
										<button type="button" class="popup-closer cross"></button>
									</div>
								</div>
							</div>
					</div>
					<!--/ popup -->
					<?php
					 endfor;
					?>
					<div class="isotope-sizer"></div>
				</div>
			</div>
			<!--/ lawyers -->
		</div>
		<?php endwhile; // End of the loop post ?>
		
		<div class="grid-col grid-col-4 grid-col-sm-12">
			<!-- practice areas -->
			<nav class="widget practice-areas">
				<h4><span>Practice Areas</span></h4>
				<?php
				$args=array("post_type"=>"expertise");
				$expertise=new WP_Query($args);
				if($expertise->have_posts()):  
   	            ?>
				<ul>
					<?php 
					 while($expertise->have_posts()):
					 	$expertise->the_post();
					    $current_id=get_the_ID();
					?>
				      <li class="<?php if($current_id==$id_post): echo 'active';  endif; ?>"><a  href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php
					 endwhile;
					?>
				</ul>
				<?php
				  endif;
				?>
			</nav>
			<!--/ practice areas -->
			
			
			<!-- testimonials -->
				<div id="testimonials" class="widget testimonials">
					<h4><span>Client Statements</span></h4>
					<div class="carousel">
						<?php
						    $args=array("post_type"=>"client_statement");
				            $clients_slideshow=new WP_Query($args);
				            if($clients_slideshow->have_posts()):
				            	while($clients_slideshow->have_posts()):
				            		$clients_slideshow->the_post();
				            	    $client_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						?>
						<div class="item">
							<div class="pic">
								<img src="<?php echo $client_image[0]; ?>" width="165" height="54" alt="">
							</div>
							<p><?php the_content(); ?></p>
							<h5><?php the_title(); ?></h5>
							<h6><?php echo get_post_meta( get_the_ID(), 'job', true ); ?></h6>
						</div>
						 <?php
					         endwhile;
					       endif;
					    ?>
					</div>
				</div>
			<!--/ testimonials -->
		</div>
	</div>
</div>
<?php
//get_sidebar();
get_footer();
