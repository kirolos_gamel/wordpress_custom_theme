<?php
get_header();
?>

<?php
    $args=array('pagename' => 'offices');
    $office_page=new WP_Query($args);
	if($office_page->have_posts()):
	   while($office_page->have_posts()):
		$office_page->the_post();
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-1.jpg'; endif; ?>)">
	<div id="headline-map" class="map"></div>
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>

<?php
/**********************************************************
I want to get all positions of all offices
***********************************************************/
$args=array("post_type"=>"office_cpt");
$office=new WP_Query($args);
if($office->have_posts()):
   $positions=array();
   $images_offices=array();
   while($office->have_posts()):
	   	$office->the_post();
	    $one_position=get_post_meta(get_the_ID(),'map', true );
	    $one_image_office = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	    array_push($positions,$one_position);
	    array_push($images_offices,$one_image_office);
    endwhile;
endif;    
?>

<?php
	while ( have_posts() ) : the_post();
?>
<div class="grid-con">
	<div class="grid-row">
		<div class="grid-col grid-col-7 grid-col-md-12">
			<!-- office -->
			<div class="block wysiwyg office">
				<h1><?php the_title(); ?></h1>
				<div class="content">
				  <?php	the_content(); ?>
				</div>
				<h2>Attorneys</h2>
				<?php
				$one_position=get_post_meta(get_the_ID(),'map', true );	 
				//var_dump($one_position);
				$newone=get_field('lawyers');
				if($newone):
				?>	
				<ul>
					<?php
					  for($i=0;$i<count($newone);$i++):
					?>
                     <li><a class="popup-opener" href="#<?php echo $newone[$i]->ID; ?>"><?php echo $newone[$i]->post_title; ?></a></li>

					<?php
	                   endfor;
	                ?>
				</ul>
                <?php
                   endif;
                ?>
			</div>
			<!--/ office -->
		</div>
		
		
		<div class="grid-col grid-col-5 grid-col-md-12">
			<!-- office -->
			<div class="block wysiwyg">
				<div id="office-map" class="map"></div>
				<h3>Contact Info</h3>
				<p><?php echo $one_position['address'] ?></p>
				<p>Tel.:<?php echo get_post_meta(get_the_ID(),'telephone', true ); ?><br>Fax: <?php echo get_post_meta(get_the_ID(),'fax', true ); ?><br>E-mail: <a href="#"><?php echo get_post_meta(get_the_ID(),'email', true ); ?></a><br>Website: <a href="#"><?php echo get_post_meta(get_the_ID(),'website', true ); ?></a></p>
			</div>
			<!--/ office -->
		</div>
	</div>
</div>





<!-- map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwuzxaNw1tAbz-grG-Bl_oAonZl9gTkMo"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/richmarker.min.js"></script>
<script>
	var map,officeMap;
	
	map = new google.maps.Map(document.getElementById('headline-map'),
	{
		zoom: 3,
		scrollwheel: false,
		disableDefaultUI: true,
		center: new google.maps.LatLng(38.2534491,31.2233592),
		styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
	});

	<?php
	for($v=0;$v<count($positions);$v++):
	?>
	new RichMarker({
		position: new google.maps.LatLng(<?php echo $positions[$v]['lat']; ?>,<?php echo $positions[$v]['lng']; ?>),
		map: map,
		flat: true,
		content: '<div class="pin"><a href="office.html"><img src="<?php echo $images_offices[$v][0]; ?>" alt=""></a></div>'
	});

	<?php
    endfor;
	?>

	officeMap = new google.maps.Map(document.getElementById('office-map'),
	{
		zoom: 15,
		scrollwheel: false,
		center: new google.maps.LatLng(<?php echo $one_position['lat']; ?>,<?php echo $one_position['lng']; ?>),

	});

	new RichMarker({
				position: new google.maps.LatLng(<?php echo $one_position['lat']; ?>,<?php echo $one_position['lng']; ?>),
				map: officeMap,
				flat: true,
				content: '<div class="pin"></div>'
			});

	
	
</script>

<?php

if($newone):
	//echo "<h1>Hoya feh eh???</h1>";
  foreach($newone as $lawyer):
  	//var_dump($lawyer);
   $image_lawyer=wp_get_attachment_image_src( get_post_thumbnail_id($lawyer->ID), 'single-post-thumbnail' );
?>
<!-- popup -->
<div id="<?php echo $lawyer->ID; ?>" class="popup popup-laweyer">
	<div class="popup-closer over"></div>
	<div class="table">
			<div class="cell">
				<div class="inner">
					<div class="grid-row">
						<div class="grid-col grid-col-3 removed-sm">
							<img src="<?php echo $image_lawyer[0]; ?>" width="195" height="270" alt="">
							<div class="wysiwyg">
								<p>T: <span><?php echo get_post_meta($lawyer->ID , 'telephone', true ); ?></span><br>E: <a href="#"><?php echo get_post_meta($lawyer->ID , 'email', true ); ?></a></p>
							</div>
						</div>
						<div class="grid-col grid-col-9 grid-col-sm-12">
							<div class="wysiwyg">
								<h2><?php echo $lawyer->post_title; ?></h2>
								<p class="content">
									<?php echo $lawyer->post_content; ?>
								</p>
								<h4>Practice Areas</h4>
								<ul>
									<?php
									    $practice_areas=wp_get_post_terms($lawyer->ID,'practice_areas');
									    //print_r($practice_areas);
									    foreach($practice_areas as $area):
									?>
                                     <li><?php echo $area->name; ?></li>
                                    <?php endforeach; ?> 
							    </ul>
							    <h4>Languages</h4>
								<p>
									<?php
									    $languages=wp_get_post_terms($lawyer->ID,'languages');
									    //print_r($practice_areas);
									    $num=count($languages);
									    $b=0;
									    foreach($languages as $language):
                                          ++$b;
									?>
                                     <?php if($b==($num)): echo $language->name; else: echo $language->name.", "; endif; ?>
                                    <?php endforeach; ?> 
							    </p>
								<h4>Publications</h4>
								<p>
								 <?php  echo get_post_meta($lawyer->ID , 'publications', true ); ?>
							    </p>
							</div>
						</div>
					</div>
					<button type="button" class="popup-closer cross"></button>
				</div>
			</div>
		</div>
</div>
<!--/ popup -->
<?php
   endforeach;
endif;
?>

<?php
endwhile;
get_footer();
?>