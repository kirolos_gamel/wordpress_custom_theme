<?php
get_header();
?>
<?php
    $args=array('pagename' => 'newsletter');
    $expertise_page=new WP_Query($args);
	if($expertise_page->have_posts()):
	   while($expertise_page->have_posts()):
		$expertise_page->the_post();		 
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>		  	
<!-- headline -->
<div id="headline" class="block headline" style="background-image: url(<?php if($image[0]): echo $image[0]; else: echo bloginfo('template_directory').'/img/headline/pic-7.jpg'; endif; ?>)">
	<div class="grid-con">
		<div class="table">
			<div class="cell">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>
<!--/ headline -->
<?php endwhile; endif; ?>


<div class="grid-con">
	<div class="grid-row">
		<div class="grid-col grid-col-8 grid-col-sm-12">
			<!-- post -->
			<?php
			while ( have_posts() ) : the_post();
		   ?>
			<div class="block wysiwyg post">
				<h1><?php the_title(); ?></h1>
				<div class="date"><?php the_date(); ?></div>
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
			<!--/ post -->
			<?php endwhile; // End of the loop. ?>
		</div>
		
		
		<div class="grid-col grid-col-4 grid-col-sm-12">
			<!-- search -->
			<form class="widget search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
				<input type="hidden" name="post_type" value="post" />
				<div class="input"><input name="s" id="s" type="text" placeholder="Search Newsletter..."></div>
				<button type="submit" class="button"><i class="fa fa-search"></i></button>
			</form>
			<!--/ search -->
			
			
			<!-- practice areas -->
			<nav class="widget practice-areas">
				<h4><span>Categories</span></h4>
				<ul>
					<li><a href="newsletter.html">Client Alerts</a></li>
					<li><a href="newsletter.html">News & Events</a></li>
					<li><a href="newsletter.html">Helpful Information</a></li>
					<li><a href="newsletter.html">We are hiring</a></li>
				</ul>
			</nav>
			<!--/ practice areas -->
			
			
			<!-- latest posts -->
			<?php
			  if(is_singular( 'post' )):
			  	$args=array('post_type' => 'post',"posts_per_page"=>4,'category_name'=>'newsletter');
					$newsletter_posts=new WP_Query($args);
					if($newsletter_posts->have_posts()):
					   
			?>
			<nav class="widget latest-posts">
				<h4><span>Latest Post</span></h4>
				<?php
				  while($newsletter_posts->have_posts()):
				  	$newsletter_posts->the_post();
				  	$image_posts_news = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				?>
				<div class="item">
					<a href="<?php the_permalink(); ?>" class="pic"><img src="<?php echo $image_posts_news[0]; ?>" width="76" height="76" alt=""></a>
					<div class="wysiwyg">
						<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						<div class="date"><?php the_date(); echo " "; the_time(); ?></div>
					</div>
				</div>
                <?php
                     endwhile;
                   endif;
                endif;   
                ?>
			</nav>
			<!--/ latest posts -->
			
			
			<!-- subscription -->
			<div class="widget subscription">
				<h4><span>Stay In Touch</span></h4>
				<p>Subscribe below to get alerts, news, info and publications from Mena Associates</p>
				<!-- <form action="#">
					<div class="input"><input type="text" placeholder="Enter your name"></div>
					<div class="input"><input type="email" placeholder="Enter your email"></div>
					<div class="note"><i class="fa fa-lock"></i>We will never share your email address</div>
					<button type="submit" class="button">Submit<i class="fa fa-angle-right"></i></button>
				</form> -->
				<?php echo do_shortcode("[caldera_form id='CF57c841eb1c706']"); ?>
			</div>
			<!--/ subscription -->
		</div>
	</div>
</div>

<?php
//get_sidebar();
get_footer();
